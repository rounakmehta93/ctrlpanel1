/**************************************************************
*
*	CtrlPanel Test
*
***************************************************************/

#include "./libBBB/libBBB.h"
#include "math.h"
#include "PID.h"

/* LIBRARIES NEEDED FOR LCM*/
#include <inttypes.h>
#include <lcm/lcm.h>
#include "lcmtypes/ctrlPanelData_t.h"  // ctrlPanel lcm type
#include "lcmtypes/pidData_t.h"  // PID lcm type

//Sensor Limit calibration
//MEASURE FOR YOUR PANEL
#define F1MIN 0
#define F1MAX 1800
#define F2MIN 0
#define F2MAX 1800
#define P1MIN 0
#define P1MAX 1800
#define P2MIN 0
#define P2MAX 1800
#define P3MIN 0
#define P3MAX 1800

//set range of pot control
//YOU NEED TO DEFINE THESE
#define KPMAX 0
#define KIMAX 0
#define KDMAX 0

struct Ctrlpanel_data{
  	int fader1;
  	int fader2;
  	int pots[3];
	int switches[3];
    	int motorSignal;
	float fader1_scaled;
	float fader2_scaled;
	float pots_scaled[3];
};

void InitializePanel(void){

 	/* Initiate Pin 66 Linked to Switch 1 */
    //INCLUDE OTHER INPUTS!
        initPin(66);
        setPinDirection(66, INPUT);

        /* Initiate Pin 45 that defines motor direction */
        initPin(45);
        setPinDirection(45, OUTPUT);
	
        /* Initiate A/D Converter */
        initADC();

        /* Initiate PWM for Servo 1 */
        initPWM(SV1);
        setPWMPeriod(SV1H, SV1, 100000);
        setPWMPolarity(SV1H, SV1, 0);
        setPWMDuty(SV1H, SV1, 0);
        setPWMOnOff(SV1H, SV1, ON);

}

/*******************************************    
*       Scaling functions to scale AD readings
*       0 to 1 for pots and -1 to 1 for faders
*******************************************/

float ScaleInt2PosFloat(int input, int min, int max){
    //scale readings to between 0 and 1
    //IMPLEMENT ME!
}

float ScaleInt2Float(int input, int min, int max){
    //scale readings to between -1 and 1
    //IMPLEMENT ME!
}

/*******************************************    
*       Read values of the sensors
*       IMPLEMENT THIS FIRST TO TEST 
*******************************************/

void ReadPanelValues(struct Ctrlpanel_data * ctrlpanel){

	/* Read ADCs and give time after each reading to settle */
	ctrlpanel->fader1 = readADC(HELPNUM, A0);
	usleep(100);
	ctrlpanel->fader2 = readADC(HELPNUM, A1);
	usleep(100);
	ctrlpanel->pots[0] = readADC(HELPNUM, A2);
	usleep(100);
    //ADD OTHER POTENTIOMETERS

	/* Scale the readings */
    /* //UNCOMMENT WHEN READY TO IMPLEMENT
	ctrlpanel->fader1_scaled = -ScaleInt2Float(ctrlpanel->fader1,F1MIN, F1MAX);	
	ctrlpanel->fader2_scaled = -ScaleInt2Float(ctrlpanel->fader2,F1MIN, F1MAX);	
	for(int i=0;i<3;i++)
		ctrlpanel->pots_scaled[i] = ScaleInt2PosFloat(ctrlpanel->pots[i],P1MIN, P1MAX);
    */

	/* Read Switches position */
	ctrlpanel->switches[0] = getPinValue(66);
    //ADD ADDITIONAL SWITCHES 

}


void PrintPanelValues(struct Ctrlpanel_data * ctrlpanel){

	// CHANGE TO PRINT ALL INPUTS AND EVENTUAL OUTPUTS YOU ALSO WANT TO CHECK
	printf("fader1: %4d\n",
	ctrlpanel->fader1);

}

/*******************************************    
*       
*       CONTROLLERS
*
*******************************************/
void SimpleFaderDriver(struct Ctrlpanel_data * ctrlpanel){
	
	/* Drive Fader1 motor with Fader 2 position */
	/* Position of Fader 2 is reflected in the speed of Fader 1 */
	/* Fader 2 in middle = Fader 1 does not move */
	/* Fader 2 left = Fader 1 moves to left up to limit */
	/* Fader 2 right = Fader 1 moves to right up to limit */
	  
	ctrlpanel->motorSignal = 200*(ctrlpanel->fader2-1700/2);
      	setPinValue(45, ctrlpanel->motorSignal > 0);
      	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));

}

void BangBang(struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
}

void PID(PID_t *pid, struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
}


/*******************************************    
*       
*       LCM Message Preparation
*
*******************************************/

void CtrlPanelDataLCM(struct Ctrlpanel_data * ctrlpanel, const char *channel, lcm_t * lcm){

        ctrlPanelData_t msg = {};
        
	    /* Prepare message to send over lcm channel*/
        msg.timestamp = utime_now();
        msg.fader1 = ctrlpanel->fader1;
        msg.fader2 = ctrlpanel->fader2;
        msg.pots[0] = ctrlpanel->pots[0];
        msg.pots[1] = ctrlpanel->pots[1];
        msg.pots[2] = ctrlpanel->pots[2];
        msg.switches[0] = ctrlpanel->switches[0];
        msg.switches[1] = ctrlpanel->switches[1];
        msg.switches[2] = ctrlpanel->switches[2];

        ctrlPanelData_t_publish(lcm, channel, &msg);
}

void PIDLCM(struct Ctrlpanel_data * ctrlpanel, PID_t *pid, const char *channel, lcm_t * lcm){
	
	pidData_t msg = {};
	
    /* Prepare message to send over lcm channel*/
    //IMPLEMENT ME!
}



/*******************************************    
*       
*       MAIN FUNCTION
* 
*******************************************/
int main()
{

	struct Ctrlpanel_data ctrlpanel;
        
	// For Controllers
	int64_t thisTime, lastTime, timeDiff;
	int updateRate = 5000;

	// PID
	//PID_t *pid;
	//pid = PID_Init(0,0,0);

        // LCM
        const char *channel1 = "CTRL_PANEL_DATA";   // LCM channel to publish messages
        const char *channel2 = "PIDDATA";   // LCM channel to publish messages
        lcm_t * lcm = lcm_create(NULL);
        if(!lcm) {
                return 1;
                }

	// Panel Initialization
	InitializePanel();

	/* Never ending loop */	
	while(1)
	{
		ReadPanelValues(&ctrlpanel);

		// Control at a specified frequency
		thisTime = utime_now();
                timeDiff = thisTime - lastTime;
                if(timeDiff >= updateRate){
			lastTime = thisTime;
			SimpleFaderDriver(&ctrlpanel);
			//BangBang(&ctrlpanel);
			//PID(pid,&ctrlpanel);
		}

		PrintPanelValues(&ctrlpanel);
		
		// UNCOMMENT TO INCLUDE LCM MESSAGE
		//CtrlPanelDataLCM(&ctrlpanel,channel1,lcm);	
		//PIDLCM(&ctrlpanel,pid,channel2,lcm);	
	
	}

	return 0;
}
